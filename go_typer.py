import idaapi
import idautils
import idc
from array import array
idaapi.require("Walker")
idaapi.require("Walker.Walker")
idaapi.require("Walker.Handlers")
idaapi.require("Walker.x86Handlers")
#Walker - https://gitlab.com/zaytsevgu/ida-walk

############################
# Simple script to automatically parse used types
# by enumerating all cals to "runtime_newobject"
# Actually in can be used also for "runtime_convT2E" at least
# It used another my script - IdaWalker which can be found by link above
# In my tests Walker create too many errors - I guess all debug prints in Walker
# can be disabled by hand. (For me only "inc error" creates too many errors, so maybe it will be enough disable only inc errors)
# Maybe someday debug output in Walker will be configurable ;[
# typer - object which returned by types_GoXXXXX() call. You'd better use same typer everywhere because it saves parsed types which 
# I think is really important
# name - name of the tracked function. Sometimes name can contain dot while IDA will shows it as _
############################

def findFunct(search):
    ea = ScreenEA()
    for funcea in Functions(SegStart(ea), SegEnd(ea)):
        name = GetFunctionName(funcea)
        if name == search:
            return funcea
    return None

def getAndCall(typer, name="runtime_newobject"):
    chain = Walker.Handlers.HandlerChain()
    coll = Walker.x86Handlers.x86Calls([name])
    chain.addVisitor(coll)
    basic = Walker.x86Handlers.x86Base()
    chain.addVisitor(basic)
    walker, conf = Walker.Walker.getMyWalker("xref", "linear")
    conf['handler'] = chain
    conf['fcn'] = findFunct(name) #ScreenEA()
    conf['trace'] = True
    conf['stackargs'] = 1
    runner = walker(conf)
    print "Start walk"
    metadata = runner.walk()
    print "End walk"
    data = ""
    visited = []
    for j in metadata:
      for i in j:
	if i['stack'][0] != 'err':
		addr = i['stack'][0]
        	print "%x" % i["pos"]
		print "%x" % addr
		typer.handle_offset(addr)
		