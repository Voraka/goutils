
from idaapi import stroffflag, offflag
import string
import random


GLOBAL_WIN_GODATA_ADDR = None

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def getSegmentByName(name):
    for i in Segments():
        if SegName(i) == name:
            return i

def rename(offset, name):
    res = MakeNameEx(offset, name, idc.SN_NOWARN)
    if res == 0:
        name = name+"_autogen_"+id_generator()
        res = MakeNameEx(offset, name, idc.SN_NOWARN)

class Sizee(object):

    def __init__(self):
        self.size = 0
        self.idatype = 0
        self.data = None

    def initType(self):
        self.types = {
            "uintptr": (self.idatype|FF_DATA, -1, self.size),
            "uint32" : (FF_DWRD|FF_DATA, -1, 4),
            "uint8"  : (FF_BYTE|FF_DATA, -1, 1),
            "ptr"    : (offflag()|FF_DATA|self.idatype, -1, 4),
        }

    def parseField(self, field):
        name = field
        if name[0] == "*":
            name = name[1:]
        if name not in self.types:
            if name != field:
                return FF_DATA, None, self.size
            return FF_DATA, None, None
        if field[0] == "*":
            return self.types["ptr"]
        return self.types[field]


class SizeDword(Sizee):

    def __init__(self):
        self.size = 4
        self.idatype = FF_DWRD
        self.data = Dword
        self.uintptr = "uint32"
        self.initType()


class SizeQword(Sizee):

    def __init__(self):
        self.size = 8
        self.idatype = FF_QWRD
        self.data = Qword
        self.initType()


def process_segment_types(handler, name=".typelink", bts=SizeDword):
    position = 0
    for i in Segments():
        if SegName(i) == name:
            position = i
            break
    if position == 0:
        raise Exception("Couldn't find segment")
    h = handler(i, SegEnd(i), step=bts)
    for i in h:
        pass
    return h


class TypeParser(object):

    def __init__(self, pos, endpos, step=SizeDword):
        self.pos = pos
        self.end = endpos
        self.stepper = step()
        self.types_id = {}

    def __iter__(self):
        return self

    def next(self):
        if self.pos >= self.end:
            raise StopIteration
        value = self.stepper.data(self.pos)
        self.pos += self.stepper.size
        return self.handle_offset(value)

    def createStruct(self, name):
        sid = GetStrucIdByName(name)
        if sid != -1:
            DelStruc(sid)
        sid = AddStrucEx(-1, name, 0)
        self.types_id[name] = sid
        return sid

    def fillStruct(self, sid, data):
        for i in data:
            #print "Creating field %s" % i[0]
            new_type = None
            (i1, i2, i3) = self.stepper.parseField(i[1])
            if i2 is None:
                name = i[1]
                if name[0] == "*":
                    name = name[1:]
                #print name
                i1,i2,i3 = (FF_BYTE|FF_DATA, -1, 1)
                if name == i[1]:
                    new_type = i[1]
                else:
                    new_type = name + " *"
                #print new_type
            res = AddStrucMember(sid, i[0], -1, i1, i2, i3)
            use_name = i[0]
            if res == -1: #Bad name
                #print "Bad name %s for struct member" % i[0]
                use_name = i[0] + "_autogen_"+id_generator()
                AddStrucMember(sid, use_name, -1, i1, i2, i3)
            if new_type is not None:
                offset = GetMemberOffset(sid, use_name)
                #print "Setting %s as %s" % (i[0], new_type)
                SetType(GetMemberId(sid, offset), new_type)

    def makeStruct(self, i):
        print "Creating structure %s" % (i[0])
        sid = self.createStruct(i[0])
        self.fillStruct(sid, i[1])

    def createTypes(self, types):
        for i in types:
            self.makeStruct(i)

    def createEnum(self, enum):
        eid = AddEnum(-1, enum[0], 0x1100000) #what is this flag?
        SetEnumBf(eid, 1)
        val = 0
        mask = 0x1f
        SetEnumWidth(eid, 1)
        for i in enum[1]:
            AddConstEx(eid, i, val, mask)
            val += 1

    def createEnums(self, enums):
        for i in enums:
            self.createEnum(i)

    def relaxName(self, name):
        name = name.replace('.','_').replace("<-",'_chan_left_').replace('*','_ptr_').replace('-','_').replace(';','').replace('"','').replace('\\','')
        name = name.replace('(','').replace(')','').replace('/','_').replace(' ','_').replace(',','comma').replace('{','').replace('}','').replace('[','').replace(']','')
        return name


class GoParser(TypeParser):
    def __init__(self, pos, endpos, step=SizeDword, createStandard = True):
        super(GoParser, self).__init__(pos, endpos, step)
        self.standardTypes = [
                              ("string",[("ptr","*char"), ("len", "uintptr")]),
                              ("slice", [("data","*char"),("len", "uintptr"), ("cap", "uintptr")]),
                              ("iface", [("itab","*char"),("ptr","*char")])
        ]

        self.commonTypes = [
                              ("arrayType",[
                                  ("type", "type"),
                                  ("elem", "*type"),
                                  ("slice", "*type"),
                                  ("len", "uintptr")
                              ]),
                              ("chanType", [
                                  ("type", "type"),
                                  ("elem", "*type"),
                                  ("dir", "uintptr")
                              ]),
                              ("ptrType", [
                                  ("type", "type"),
                                  ("elem", "*type")
                              ]),
                              ("sliceType", [
                                  ("type", "type"),
                                  ("elem", "*type")
                              ])                              
        ]
        self.standardEnums = [
            ("kind",[
            "INVALID", "BOOL","INT","INT8",
            "INT16","INT32","INT64","UINT",
            "UINT8","UINT16","UINT32","UINT64",
            "UINTPTR","FLOAT32","FLOAT64","COMPLEX64",
            "COMPLEX128","ARRAY","CHAN","FUNC","INTERFACE","MAP","PTR","SLICE",
            "STRING","STRUCT","UNSAFE_PTR"
            ])
        ]
        self.kind_types = {
            "CHAN": self.makeChanType,
            "ARRAY": self.makeArrType,
            "SLICE": self.makeSliceType,
            "STRUCT": self.makeStructType,
            "PTR"  : self.makePtrType,
            "INTERFACE": self.makeInterface,
            "FUNC":  self.makeFunc,
            "MAP": self.makeMap,
        }

        self.kind_size = {
        }
        self.type_addr = []
        if createStandard:
            self.createTypes(self.standardTypes)
            self.createEnums(self.standardEnums)

    def getPtr(self, sid, addr, name):
        name_off = GetMemberOffset(sid, name)
        return self.stepper.data(addr+name_off)

    def getDword(self, sid, addr, name):
        name_off = GetMemberOffset(sid, name)
        return Dword(addr+name_off)

    def getName(self, offset):
        sid = GetStrucIdByName("type")
        string_addr = offset + GetMemberOffset(sid, "string")
        ptr = self.stepper.data(string_addr)
        SetType(ptr, "string")
        name = self.stepper.data(ptr)
        return GetString(name)
    
    def nameFromOffset(self, offset):
        #print offset
        offset = self.stepper.data(offset)
        return GetString(offset)

    def getOffset(self, offset):
        return offset

    def getPtrToThis(self, sid, offset):
        return self.getPtr(sid, offset, "ptrtothis")

    def make_arr(self, addr, arr_size, struc_size, type):
        res = MakeArray(addr, arr_size)
        if res == False:
            MakeUnknown(addr, arr_size*struc_size, DOUNK_SIMPLE)
            SetType(addr, type) 
            MakeArray(addr, arr_size)    

    def handle_offset(self, offset):
        if offset in self.type_addr:
            return
        print "Processing: %x" % offset
        self.type_addr.append(offset)
        SetType(offset, "type")
        name = self.getName(offset)
        MakeComm(offset, name)
        struc_id = GetStrucIdByName("type")
        offset_kind = GetMemberOffset(struc_id, "kind")
        kind = Byte(offset + offset_kind) & 0x1f
        kind_name = self.getKindEnumName(kind)
        if name[0] == "*" and kind_name != "PTR":
	        name = name[1:]		
        name = self.relaxName(name)
        rename(offset, name)
        self.betterTypePlease(offset)
        sid = GetStrucIdByName("type")
        addr = self.getPtrToThis(sid, offset)
        #print "Got following ptr addr: %x" % addr
        if addr != 0:   
            addr = self.getOffset(addr)
            self.handle_offset(addr) 
        #print "type %x processed" % offset  
        if kind_name != "FUNC":
            self.processUncommon(sid, offset)

    def processUncommon(self, sid, offset):
        uncommon_offset = GetMemberOffset(sid,"UncommonType")
        unc_addr = self.getPtr(sid,offset, "UncommonType")
        unc_id = GetStrucIdByName("uncommonType")
        if unc_addr != 0:
            SetType(unc_addr, "uncommonType")  
            SetType(self.getPtr(unc_id, unc_addr, "name"), "string")
            SetType(self.getPtr(unc_id, unc_addr, "pkgPath"), "string")
            offs = GetMemberOffset(unc_id, "methods")
            slice_id = GetStrucIdByName("slice")
            size_of = GetMemberOffset(slice_id, "len")
            size = self.stepper.data(unc_addr+offs+size_of)
            if size > 0:
                addr = self.getPtr(slice_id, unc_addr+offs, "data")
                SetType(addr, "method__")
                #print "Got size %d" % size
                sz = GetStrucSize(GetStrucIdByName("method__"))
                self.make_arr(addr, size, sz, "method__")

    def getKindEnumName(self, constant):
        return self.standardEnums[0][1][constant]

    def betterTypePlease(self, offset):
        #print "Better type processing at %x" % offset
        struc_id = GetStrucIdByName("type")
        offset_kind = GetMemberOffset(struc_id, "kind")
        kind = Byte(offset + offset_kind) & 0x1f
        kind_name = self.getKindEnumName(kind)
        if kind_name in self.kind_types:
            self.kind_types[kind_name](offset)
        #print "Better type processing at %x finished" % offset
    
    def makeChanType(self, offset):
        SetType(offset, "chanType")
        sid = GetStrucIdByName("chanType")
        addr = self.getPtr(sid, offset, "elem")
        self.handle_offset(addr)

    def makeSliceType(self, offset):
        SetType(offset, "sliceType")
        sid = GetStrucIdByName("sliceType")
        addr = self.getPtr(sid, offset, "elem")
        self.handle_offset(addr)

    def makeArrType(self, offset):
        SetType(offset, "arrayType")
        sid = GetStrucIdByName("arrayType")
        addr = self.getPtr(sid, offset, "elem")
        self.handle_offset(addr)
        addr = self.getPtr(sid, offset, "slice")
        self.handle_offset(addr)

    def makePtrType(self, offset):
        #print "Processing ptr"
        SetType(offset, "ptrType")
        sid = GetStrucIdByName("ptrType")
        addr = self.getPtr(sid, offset, "elem") 
        #print "Got pointed adddr %x" % addr
        self.handle_offset(addr)
        #print "Pointed processed"

    def makeStructType(self, offset):
        #print "Making struct"
        debug = offset == 0x7A90A0
        SetType(offset, "structType")
        sid = GetStrucIdByName("structType")
        slice_id = GetStrucIdByName("slice")
        offset_elem = GetMemberOffset(sid, "fields")
        inner_offset = GetMemberOffset(slice_id, "data")
        addr = self.stepper.data(offset_elem + offset + inner_offset)

        inner_offset = GetMemberOffset(slice_id, "len")
        size = self.stepper.data(offset+offset_elem+inner_offset)
        if debug == True:
            print "Size is %d" % size
        if size == 0:
            return
        SetType(addr, "structField")
        sz = GetStrucSize(GetStrucIdByName("structField"))
        self.make_arr(addr, size, sz, "structField")
        sid_type = GetStrucIdByName("type")
        size_new_struct = self.getPtr(sid_type, offset, "size")
        for i in xrange(size):
            if debug is True:
                print "Processing %d" % i
            self.processStructField(addr, i*sz)
        name = self.getName(offset)
        if debug == True:
            print "Name is %s" % name
        while name[0] == "*":
            name = name[1:]
        name = self.relaxName(name)
        name = "ut_" + name
        self.createUserTypeStruct(addr, name, size, size_new_struct)
         
    def createUserTypeStruct(self, addr, name, size, self_size):
        fields = []
        #print name
        sid = GetStrucIdByName("structField")
        sz = GetStrucSize(sid)
        sid_type = GetStrucIdByName("type")
        fields = []
        curr_offset = 0
        MakeComm(addr, name)
        for i in xrange(size):
            fieldname = self.nameFromOffset(self.getPtr(sid, addr+i*sz,"Name"))
            #print fieldname
            type_addr = self.getPtr(sid, addr+i*sz, "typ")
            typename = self.getType(type_addr)
            size = self.getPtr(sid_type, type_addr, "size")
            #print typename
            if fieldname == "" or fieldname is None:
                fieldname = "unused_"+id_generator()
            offset = self.getPtr(sid, addr+i*sz, "offset")
            if offset != curr_offset:
                #print name
                #print fieldname
                print "Offset missmatch.Got %d expected %d. Adding padding..." % (curr_offset, offset)
                if offset < curr_offset:
                    raise("Too many bytes already")
                while offset != curr_offset:
                    fields.append(("padding", "char"))
                    curr_offset += 1
            curr_offset += size
            if size != 0:
                offset_kind = GetMemberOffset(sid_type, "kind")
                kind = Byte(type_addr + offset_kind) & 0x1f
                kind_of_type = self.getKindEnumName(kind)
                if kind_of_type == "STRUCT_": #Disabled for now
                    name_type = self.getName(type_addr)
                    while name_type[0] == "*":
                        name_type = name_type[1:]
                    name_type = self.relaxName(name_type)
                    name_type = "ut_" + name_type
                    #print "setting type %s" % name_type
                    fields.append((fieldname, name_type))
                elif kind_of_type == "STRING":
                    fields.append((fieldname, "string"))
                elif kind_of_type == "SLICE":
                    fields.append((fieldname, "slice"))
                elif kind_of_type == "INTERFACE":
                    fields.append((fieldname, "iface"))
                else:
                    fields.append((fieldname, "char [%d]" % size))
        if curr_offset != self_size:
            print "%x: Structure size mismatch: %x" % (addr, curr_offset)
            if self_size < curr_offset:
                    raise("Too many bytes already")
            while self_size != curr_offset:
                fields.append(("padding", "char"))
                curr_offset += 1    
        new_type = [(name, fields)]
        #print new_type
        self.createTypes(new_type)
        #print fields
        new_type_sid = GetStrucIdByName(name)
        sz = GetStrucSize(new_type_sid)
        if sz != self_size:
            print "%x" % addr   
            raise("Error at creating structure")

    
    def getType(self, addr):
        sid = GetStrucIdByName("type")
        kind = Byte(addr+GetMemberOffset(sid,"kind")) & 0x1f
        name = self.getName(addr)
        if self.getKindEnumName(kind) != "PTR":
            while name[0] == "*":
                name = name[1:]
        return name

    def get_kind(self, addr):
        sid = GetStrucIdByName("type")
        kind = Byte(addr+GetMemberOffset(sid,"kind")) & 0x1f
        return self.getKindEnumName(kind)
    
    def processStructField(self, addr, index):
        #print "Processing field %x" % addr
        offset = addr + index
        sid = GetStrucIdByName("structField")
        ptr = self.getPtr(sid, offset, "Name")
        if ptr != 0:
            SetType(ptr, "string")
            fieldName = GetString(self.stepper.data(ptr))
            rename(ptr, fieldName)
        ptr = self.getPtr(sid, offset, "typ")
        if True:
        #if self.get_kind(ptr) != "PTR":
            #TODO: fix
            """ This is ugly hack because of following situation:
                struct a {
                    f1: int
                    f2: *b
                }
                struct b {
                    f1: int
                    f2: a
                }
                this will break everything

                I guess this will be fixed in rewrited version
            """ 
            self.handle_offset(ptr)

    def makeInterface(self, offset):
        SetType(offset, "interfaceType")
        ifaceid = GetStrucIdByName("interfaceType")
        meth_offs = GetMemberOffset(ifaceid,"methods")
        slice_id = GetStrucIdByName("slice")
        size_off = GetMemberOffset(slice_id, "len")
        size = self.stepper.data(offset+meth_offs+size_off)
        if size != 0:
            addr = self.getPtr(slice_id, offset+meth_offs, "data")
            SetType(addr, "imethod")
            sz = GetStrucSize(GetStrucIdByName("imethod"))
            self.make_arr(addr, size, sz, "imethod")
            names = self.processIMethods(addr, size)
            #For now only for go1.7
            if names is None:
                return
            name = self.getName(offset)
            while name[0] == "*":
                name = name[1:]
            name = self.relaxName(name)
            name = "user_interface_" + name
            # TODO: this is for go1.7 need additional check for other versions
            fields = [("inter", "void *"),("type","void *"),("link","void *"),("bad","__int32"), ("unused","__int32")] 
            for i in names:
                fields.append((i, "void *"))
            itype = [(name, fields)]
            self.createTypes(itype)

    def processIMethods(self, addr, size):
        return None

    def makeFunc(self, offset):
        SetType(offset,"funcType")
        self.parseFuncType(offset)
    
    def parseFuncType(self,offset):
        return

    def makeMap(self, offset):
        return #TODO:fix

#This class created structures that common for golang < 1.7
class GoTypes(GoParser):

    def __init__(self, pos, endpos, step=SizeDword, createStandard = True):
        super(GoTypes, self).__init__(pos, endpos, step, createStandard)
        self.standardTypes = [
                              ("uncommonType", [("name", "*string"), ("pkgPath", "*string"), ("methods", "slice")]),
        ]
        #this types depends on type structure so should be created after
        self.commonTypes += [
                            ("method__", [("name", "*string"),("pkgPath","*string"),("mtype","*type"),("typ","*type"),("ifn", "void *"),("tfn","void *")]),
                            ("structField",[
                                    ("Name",   "*string"),
                                    ("PkgPath","*string"),
                                    ("typ", "*type"),
                                    ("tag", "*string"),
                                    ("offset", "uintptr"),
                              ]),
                              ("structType", [
                                    ("type","type"),
                                    ("fields", "slice")
                              ]),
                              ("imethod", [
                                  ("name", "*string"),
                                  ("pkgPath", "*string"),
                                  ("typ", "*type")
                              ]),
                              ("interfaceType",[
                                  ("type", "type"),
                                  ("methods", "slice")
                              ]),
                              ("funcType",[("type","type")]), #TODO:fix
                             ]
        if createStandard:
            self.createTypes(self.standardTypes)


class Go17Types(GoParser):
    def __init__(self, pos, endpos, step=SizeDword, createStandard = True):
        super(Go17Types, self).__init__(pos, endpos, step, createStandard)
	self.standardTypes = [
            ("type", [
                ("size",        "uintptr"),
                ("ptrdata",     "uintptr"),
                ("hash",        "uint32"),
                ("flag",        "uint8"),
                ("align",       "uint8"),
                ("fieldAlign",  "uint8"),
                ("kind",        "kind"),
                ("alg",         "*void"),
                ("gcdata",      "*unsigned char"),
                ("string",      "__int32"),
                ("ptrtothis",   "__int32"), 
           ])
        ]

        #this types depends on type structure so should be created after
        self.commonTypes += [
		       	            ("uncommonType", [("pkgPath", "__int32"), ("mcount", "__int16"), ("unused1", "__int16"),("moff", "__int32"), ("unused2", "__int16")]),
                            ("method__", [("name", "__int32"), ("mtyp", "__int32"),("ifn","__int32"), ("tfn", "__int32")]),
                            ("structField",[
                                    ("Name",   "void *"),
                                    ("typ", "*type"),
                                    ("offset", "uintptr"),
                              ]),
                              ("structType", [
                                    ("type","type"),
                                    ("pkgPath", "void *"),
                                    ("fields", "slice")
                              ]),
                              ("imethod", [
                                  ("name", "__int32"),
                                  ("pkgPath", "__int32"),
                              ]),
                              ("interfaceType",[
                                  ("type", "type"),
                                  ("pkgPath", "void *"),
                                  ("methods", "slice")
                              ]),
                              ("funcType", [
                                  ("type", "type"),
                                  ("incount","__int16"),
                                  ("outcount", "__int16")
                              ]),
                              ("mapType", [
                                  ("type", "type"),
                                  ("key","*type"),
                                  ("elem","*type"),
                                  ("bucket", "*type"),
                                  ("hmap", "*type"),
                                  ("keysize","uint8"),
                                  ("indirectkey","uint8"),
                                  ("valuesize","uint8"),
                                  ("indirectvalue","uint8"),
                                  ("bucketsize","__int16"),
                                  ("reflexivekey","uint8"),
                                  ("needkeyupdate","uint8"),                                                                    
                              ])
                             ]
        if createStandard:
            self.createTypes(self.standardTypes)
            self.createTypes(self.commonTypes)
        self.type_base = getSegmentByName(".rodata")
        self.text_addr = getSegmentByName(".text")

    def next(self):
        #print "next type from typelink"
        if self.pos >= self.end:
            raise StopIteration
        value = Dword(self.pos)
        self.pos += 4
        value = self.getOffset(value)
        return self.handle_offset(value)

    def getOffset(self, offset):
        return self.type_base + offset
    
    def nameFromOffset(self, offset):
        addr = offset
        return self.get_str(addr+3, Byte(addr+2))

    def getPtrToThis(self, sid, offset):
        memb_offs = GetMemberOffset(sid, "ptrtothis")
        return Dword(offset+memb_offs)
    
    def processStructField(self, addr, index):
        offset = addr + index
        sid = GetStrucIdByName("structField")
        ptr = self.getPtr(sid, offset, "Name")
        ln = Byte(ptr+2)
        fieldName = self.get_str(ptr+3, ln)
        rename(ptr, fieldName)
        ptr = self.getPtr(sid, offset, "typ")
        self.handle_offset(ptr)

    def get_str(self, pos, len):
        out = ""
        for i in xrange(len):
            out += chr(Byte(pos+i))
        return out

    def getName(self, offset):
        #print "GetName: %x" % offset
        sid = GetStrucIdByName("type")
        name_off = self.getDword(sid, offset, "string")
        string_addr = self.getOffset(name_off) + 3
        ln = Byte(string_addr-1)
        return self.get_str(string_addr, ln)

    def processUncommon(self, sid, offset):
        if GetType(offset) =="type":
            return
        uncommon_offset = GetMemberOffset(sid,"flag")
        flg = Byte(offset+ uncommon_offset)
        if flg & 1 != 0:
            #print GetType(offset)
            sz = GetStrucSize(GetStrucIdByName(GetType(offset)))
            SetType(offset+sz,"uncommonType")
            un_of = offset+sz
            und_sid = GetStrucIdByName("uncommonType")
            #make commentary with package
            pkg_off = GetMemberOffset(und_sid, "pkgPath")
            val = Dword(un_of+pkg_off)
            if val != 0:
                val += self.type_base
                ln = Byte(val+2)
                pkg = self.get_str(val+3, ln)
                MakeComm(offset+sz, pkg)
            #create methods array if any
            count_off = GetMemberOffset(und_sid,"mcount")
            count = Word(un_of + count_off)
            if count != 0:
                off_off = GetMemberOffset(und_sid, "moff")
                size = GetMemberSize(und_sid, off_off)
                if size == 2:
		            f = Word
                elif size == 4:
		            f = Dword
                elif size == 8:
		            f = Qword
                offf = f(un_of+off_off)
                SetType(un_of+offf, "method__")
                sz = GetStrucSize(GetStrucIdByName("method__"))
                self.make_arr(un_of+offf, count, sz, "method__")
                methods_off = un_of+offf
                
                comm = []
                for i in xrange(count):
                    comm.append(self.processMethods(methods_off+i*sz))
                MakeComm(methods_off,"\n".join(comm))

    def processIMethods(self, offst, size):
        sz = GetStrucSize(GetStrucIdByName("imethod"))
        comm = []
        for i in xrange(size):
            comm.append(self.processIMethod(offst+i*sz))
        MakeComm(offst, "\n".join(comm))
        return comm

    def processIMethod(self, offst):
        sid = GetStrucIdByName("imethod")
        name = self.getDword(sid, offst,"name")
        name += self.type_base
        name = self.get_str(name+3, Byte(name+2))
        return name

    def processMethods(self, offst):
        sid = GetStrucIdByName("method__")
        name = self.getDword(sid, offst, "name")
        name += self.type_base
        name = self.get_str(name+3, Byte(name+2))
        type_meth = self.getDword(sid, offst, "mtyp")
        type_meth_addr1 = self.type_base + type_meth
        func_body1 = self.getDword(sid, offst, "ifn")
        func_addr1 = self.text_addr + func_body1
        func_body2 = self.getDword(sid, offst, "tfn")
        func_addr2 = self.text_addr + func_body1
        return "%s %x %x %x" % (name, type_meth_addr1, func_addr1, func_addr2)

    def makeMap(self, offset):
        SetType(offset, "mapType")
        sid = GetStrucIdByName("mapType")
        addr = self.getPtr(sid, offset, "key")
        self.handle_offset(addr)
        addr = self.getPtr(sid, offset, "elem")
        self.handle_offset(addr)
        addr = self.getPtr(sid, offset, "bucket")
        self.handle_offset(addr)
        addr = self.getPtr(sid, offset, "hmap")
        self.handle_offset(addr)

    def parseFuncType(self, offset):
        return
        #print "func off %x" % offset
        sid = GetStrucIdByName("funcType")
        in_size = Word(offset+GetMemberOffset(sid,"incount"))
        out_size = Word(offset+GetMemberOffset(sid,"outcount"))
        sz = GetStrucSize(sid)
        for i in xrange(in_size+out_size):
            SetType(offset+sz+i*self.stepper.size, "type *")


class Go17TypesWin(Go17Types):
    def __init__(self, pos, endpos, step=SizeDword, createStandard = True):
        super(Go17TypesWin, self).__init__(pos, endpos, step, createStandard)
        #print GLOBAL_WIN_GODATA_ADDR
        self.type_base = GLOBAL_WIN_GODATA_ADDR


class Go12Types(GoTypes):

    def __init__(self, pos, endpos, step=SizeDword, createStandard=True):
        super(Go12Types, self).__init__(pos, endpos, step, createStandard)
        self.types = [
            ("type", [
                ("size",       "uintptr"),
                ("hash",       "uint32"),
                ("_unused",    "uint8"),
                ("align",      "uint8"),
                ("fieldAlign", "uint8"),
                ("kind",       "kind"),
                ("alg",        "*void"),
                ("gc",         "ptr"),
                ("string",     "*string"),
                ("UncommonType","*int"),
                ("ptrtothis",   "*type"),
            ]),
        ]
        if createStandard:
            self.createTypes(self.types)
            self.createTypes(self.commonTypes)

class Go14Types(GoTypes):

    def __init__(self, pos, endpos, step=SizeDword, createStandard=True):
        super(Go14Types, self).__init__(pos, endpos, step, createStandard)
        self.types = [
           ("type", [
                ("size",        "uintptr"),
                ("hash",        "uint32"),
                ("_unused",     "uint8"),
                ("align",       "uint8"),
                ("fieldAlign",  "uint8"),
                ("kind",        "kind"),
                ("alg",         "*void"),
                ("gcdata",      "void *[2]"),
                ("string",      "*string"),
                ("UncommonType","*uncommonType"),
                ("ptrtothis",   "*type"),
                ("zero",        "void *")
           ]),
        ]
        if createStandard:
            self.createTypes(self.types)
            self.createTypes(self.commonTypes)

class Go15Types(GoTypes):

    def __init__(self, pos, endpos, step=SizeDword, createStandard=True):
       super(Go15Types, self).__init__(pos, endpos, step, createStandard)
       self.types = [
           ("type", [
                ("size",        "uintptr"),
                ("ptrdata",     "uintptr"),
                ("hash",        "uint32"),
                ("_unused",     "uint8"),
                ("align",       "uint8"),
                ("fieldAlign",  "uint8"),
                ("kind",        "kind"),
                ("alg",         "*void"),
                ("gcdata",      "*unsigned char"),
                ("string",      "*string"),
                ("UncommonType","*uncommonType"),
                ("ptrtothis",   "*type"), 
                ("zero",        "void *")
           ])
       ]
       if createStandard:
            self.createTypes(self.types)
            self.createTypes(self.commonTypes)

class Go17_diff_uncommon(Go17Types):

    def __init__(self, pos, endpos, step=SizeDword, createStandard = True):
        super(Go17_diff_uncommon, self).__init__(pos, endpos, step, createStandard)
        #this types depends on type structure so should be created after
        self.commonTypes += [
		       	        ("uncommonType",[("name","__int32"),("mcount","__int16"),("moff","__int16")]),
                             ]
        if createStandard:
            self.createTypes(self.commonTypes)


class Go16Types(GoTypes):

    def __init__(self, pos, endpos, step=SizeDword, createStandard=True):
       super(Go16Types, self).__init__(pos, endpos, step, createStandard)
       self.types = [
           ("type", [
                ("size",        "uintptr"),
                ("ptrdata",     "uintptr"),
                ("hash",        "uint32"),
                ("_unused",     "uint8"),
                ("align",       "uint8"),
                ("fieldAlign",  "uint8"),
                ("kind",        "kind"),
                ("alg",         "*void"),
                ("gcdata",      "*unsigned char"),
                ("string",      "*string"),
                ("UncommonType","*uncommonType"),
                ("ptrtothis",   "*type"), 
           ])
       ]
       if createStandard:
            self.createTypes(self.types)
            self.createTypes(self.commonTypes)



def typesGo16_64():
    return process_segment_types(Go16Types, bts=SizeQword)

def typesGo16_32():
    return process_segment_types(Go16Types, bts=SizeDword)

def typesGo14_64():
    return process_segment_types(Go14Types, bts=SizeQword)

def typesGo15_64():
    return process_segment_types(Go15Types, bts=SizeQword)

def typesGo17_64():
    return process_segment_types(Go17Types, bts=SizeQword)

def typesGo17_32():
    return process_segment_types(Go17Types, bts=SizeDword)

def typesGo12_32():
    return process_segment_types(Go12Types, bts=SizeDword)

def typesGo17_unc_64():
    return process_segment_types(Go17_diff_uncommon, bts=SizeQword)

def typesGo16_macho_64():
    return process_segment_types(Go16Types, "__typelink", SizeQword)

def typesGo17_win32(begin_typeinfo, end_typeinfo, rodata_addr):
    global GLOBAL_WIN_GODATA_ADDR
    GLOBAL_WIN_GODATA_ADDR = rodata_addr
    h = Go17TypesWin(begin_typeinfo, end_typeinfo, step=SizeDword)
    for i in h:
        pass
    return h

def typesGo17_win64(begin_typeinfo, end_typeinfo, rodata_addr):
    global GLOBAL_WIN_GODATA_ADDR
    GLOBAL_WIN_GODATA_ADDR = rodata_addr
    h = Go17TypesWin(begin_typeinfo, end_typeinfo, step=SizeQword)
    for i in h:
        pass
    return h

def typesGo16_win32(begin_typeinfo, end_typeinfo):
    h = Go16Types(begin_typeinfo, end_typeinfo, step=SizeDword)
    for i in h:
        pass    
    return h

def typesGo16_win64(begin_typeinfo, end_typeinfo):
    h = Go16Types(begin_typeinfo, end_typeinfo, step=SizeQword)
    for i in h:
        pass    
    return h

def typesWinLessGo17_32(begin_typeinfo, end_typeinfo, types):
    h = types(begin_typeinfo, end_typeinfo, step=SizeDword)
    for i in h:
        pass
    return h

def typesWinLessGo17_64(begin_typeinfo, end_typeinfo, types):
    h = types(begin_typeinfo, end_typeinfo, step=SizeQword)
    for i in h:
        pass
    return h    